import sys
from importlib import import_module

from nucleus.apps.config import AppConfig
from nucleus.utils.imports import list_submodules, join

BASE_APP_MODULE = 'app'


class AppsRegistry:
    def __init__(self, installed_apps=()):
        if installed_apps is None and hasattr(sys.modules[__name__], 'apps'):
            raise RuntimeError("You must supply an installed_apps argument.")

        self.apps_configs = {}

        self.ready = False

        if installed_apps is not None:
            self.populate(installed_apps)

    def populate(self, installed_apps):
        if self.ready:
            return

        for app_module_name in installed_apps:
            module = import_module(app_module_name)
            submodules = list_submodules(app_module_name, ignore_names=['__init__'])

            if BASE_APP_MODULE not in submodules:
                continue

            base_app_module = import_module(join(app_module_name, BASE_APP_MODULE))
            attr_object = None
            for attr in dir(base_app_module):
                attr_object = getattr(base_app_module, attr)
                if attr_object == AppConfig:
                    continue
                if not isinstance(attr_object, AppConfig):
                    break
            if attr_object is None:
                break
            app_config = attr_object(app_module_name, module)
            setattr(base_app_module, attr, app_config)
            if app_config.name in self.apps_configs:
                raise RuntimeError(f"Nucleus can't run application with duplicate names."
                                   f"\nApp'{app_config.name}' from '{app_module_name}'")
            app_config.setup()
            self.apps_configs[app_config.name] = module
        self.ready = True

    def get_app_config(self, app_name):
        if app_name in self.apps_configs:
            return self.apps_configs[app_name]
        raise RuntimeError(f"Application '{app_name}' is not installed!")


apps = AppsRegistry(None)
