"""
Copyright © 2017 Alex Root Junior.

Licensing with LGPLv3: https://www.gnu.org/licenses/lgpl.txt
"""
from nucleus.apps.config import AppConfig

from nucleus.utils.version import get_version

VERSION = (0, 1, 0, 'alpha', 1)
__version__ = get_version(VERSION)


def setup():
    from nucleus.core.base.loader import start
    start()


if __name__ == '__main__':
    setup()
