LOGGING = {
    "LOG_LEVEL": "INFO",
    "SETUP_LOGGERS": [
        "Nucleus"
    ],
    "REPORTS_STORAGE": None,
    "HANDLERS": [
        {
            "HANDLER": "logging.StreamHandler",
            "FORMATTER": '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
        }
    ]
}


