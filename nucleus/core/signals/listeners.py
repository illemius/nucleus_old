from nucleus.core.signals.base.listener import SignalsListener


class SimpleListener(SignalsListener):
    """
    Provide using functions based event listener
    """

    def __init__(self, callback):
        self.callback = callback

    def execute(self, data):
        return self.callback(data)
