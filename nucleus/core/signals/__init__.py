from nucleus.core.signals.base.router import SignalsRouter
from nucleus.core.signals.base.system import CoreSignalsHandler

CORE_TRIGGER = 0

root_router = SignalsRouter()
core_handler = CoreSignalsHandler()

root_router.subscribe(CORE_TRIGGER, core_handler)


SIG_EXIT = 0
SIG_START = 1
SIG_STOP = 2
