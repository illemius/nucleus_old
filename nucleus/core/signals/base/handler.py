from nucleus.core.signals.base.listener import SignalsListener


class SignalsHandler:
    def __init__(self):
        self._listeners = []
        self.counter = 0

    def subscribe(self, listener: SignalsListener):
        if not isinstance(listener, SignalsListener):
            raise RuntimeError(f"listener must be an EventListener not {type(listener)}")
        self._listeners.append(listener)

    def notify_all(self, data):
        for listener in self._listeners:
            listener.execute(data)
        self.counter += 1
