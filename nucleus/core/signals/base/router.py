from nucleus.core.signals.base.handler import SignalsHandler


class SignalsRouter:
    def __init__(self):
        self._handlers = []
        self.counter = 0

    def subscribe(self, trigger, handler):
        if not isinstance(handler, SignalsHandler):
            raise RuntimeError(f"handler must be an EventListener not {handler.__name__}")
        self._handlers.append((trigger, handler))

    def trigger(self, trigger, data):
        for trig, handler in self._handlers:
            if trig == trigger:
                handler.notify_all(data)
        self.counter += 1
