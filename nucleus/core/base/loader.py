import nucleus
from nucleus.apps.registry import apps
from nucleus.conf.loader import settings
from nucleus.core.signals import root_router, CORE_TRIGGER, SIG_START, SIG_STOP


def start():
    app_name = settings.get("APPLICATION_NAME", "<NoName>")
    print(f"Nucleus v{nucleus.__version__}")
    print(f"Start application: '{app_name}'")
    apps.populate(settings['INSTALLED_APPS'])
    root_router.trigger(CORE_TRIGGER, SIG_START)


def stop():
    root_router.trigger(CORE_TRIGGER, SIG_STOP)
