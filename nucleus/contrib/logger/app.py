from NucleusUtils.logging.dumps import set_dumps_path

from nucleus.apps.config import AppConfig
from nucleus.conf.loader import settings as system_settings
from . import setup


class Config(AppConfig):
    def ready(self):
        settings = system_settings["LOGGING"]

        setup.setup_level(settings.get('LOG_LEVEL', 'INFO'))
        setup.setup_handlers(settings.get('HANDLERS', []))
        setup.setup_loggers(settings.get('SETUP_LOGGERS', []))
        set_dumps_path(settings.get('REPORTS_STORAGE'))
