# Config sample
```python
LOGGING = {
    "LOG_LEVEL": "INFO",
    "SETUP_LOGGERS": [
        "app",
        "dumps",
        "TeleBot",
        "WebHookServer"
    ],
    "REPORTS_STORAGE": os.path.join(BASE_DIR, 'reports'),
    "HANDLERS": [
        {
            "HANDLER": "logging.StreamHandler",
            "FORMATTER": '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
        }
    ]
}
```