import logging
import threading

from app import settings
from nucleus.contrib.telegram.telebot import TelegramBot
from nucleus.contrib.telegram.webhook import WebhookServer

log = logging.getLogger('TeleBot')


class BotManager:
    def __init__(self, settings):
        self._settings = settings
        self.bots = {}
        webhook = self._settings.get("WEBHOOK", {})
        self.webhook = WebhookServer(webhook.get("HOST"), webhook.get("PORT"), webhook.get("URL_BASE"))
        if webhook.get("ENABLED", False):
            self.webhook.start()

    def read_bots(self):
        bots = self._settings.get('BOTS', [])
        for name, bot_settings in bots.items():
            bot = TelegramBot(token=bot_settings.get('TOKEN'), skip_pending=bot_settings.get('SKIP_PENDING', False))
            bot.remove_webhook()
            if bot_settings.get("USE_WEBHOOK", False):
                if bot.skip_pending:
                    updates_count = bot.skip_updates()
                    if updates_count:
                        log.info('Skipped {} pending messages'.format(updates_count))
                    self.webhook.register_bot(bot)
            else:
                threading.Thread(name=f'@{bot.me.username}:BotPolling', target=bot.polling,
                                 kwargs={'none_stop': True}).start()

    def get_bot(self, name=None) -> TelegramBot:
        if name is None and len(self.bots) > 0:
            return [bot for bot in self.bots.values()][0]
        if name in self.bots:
            return self.bots[name]
        raise KeyError(name)

    def __getitem__(self, item):
        return self.get_bot(item)

bots = BotManager(settings.TELEBOT)
bots.read_bots()
