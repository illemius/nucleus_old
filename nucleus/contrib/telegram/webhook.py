import json
import logging
import threading
import uuid
from http.server import HTTPServer, BaseHTTPRequestHandler

import telebot

bots = {}
log = logging.getLogger('WebHookServer')


class WebhookServer:
    def __init__(self, host='', port=443, url_base=None):
        self.server_address = (host, port)
        self.url_base = url_base

        self.httpd = HTTPServer(self.server_address, WebHook)
        self.thread = threading.Thread(target=self.httpd.serve_forever, name='WebhookServer')

    def register_bot(self, bot, path=None):
        assert self.thread.is_alive(), 'WebHook server is not started!'

        if path is None:
            path = str(uuid.uuid4())

        if path in bots:
            raise KeyError(path)
        bots[path] = bot
        bot.set_webhook(self.url_base.rstrip('/') + '/' + path)
        log.info('Register webhook: ' + bot.webhook_url)

    def start(self):
        log.info(
            f"Started WebHook server at 'https://{self.server_address[0] or 'localhost'}:{self.server_address[1]}'")
        self.thread.start()

    def stop(self):
        self.httpd.server_close()


class WebHook(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super(WebHook, self).__init__(*args, **kwargs)

    def do_GET(self):
        if self.path == '/list':
            content = []
            for key, bot in bots.items():
                content.append(
                    f"<a href='/{key}'>{key}</a> <a href='http://t.me/{bot.me.username}'>@{bot.me.username}</a>")
            return self.response('<br />'.join(content))
        if self.path.lstrip('/') in bots:
            bot = bots.get(self.path.lstrip('/'))
            return self.redirect(f"https://t.me/{bot.me.username}")
            # return self.response(f'<a href="http://t.me/{bot.me.username}">@{bot.me.username}</a>')
        return self.send_error(404)

    def do_POST(self):
        content_length = int(self.headers.get('Content-Length', 0))
        body = self.rfile.read(content_length).decode('utf-8', 'ignore')
        self.read_data(body)
        self.response()

    def response(self, content='', code=200, content_type='text/html'):
        try:
            self.send_response(code)
            self.send_header('Content-type', content_type)
            self.end_headers()

            # Write content as utf-8 data
            self.wfile.write(bytes(content, "utf8"))
        except:
            return False
        else:
            return True

    def redirect(self, to, status=301):
        self.send_response(status)
        self.send_header('Location', f"{to}")
        self.end_headers()

    def read_data(self, data):
        try:
            json.loads(data)
        except Exception as e:
            log.error("Wrong data: '{}'. Raise: {}".format(data, e))
        else:
            log.debug("The server call: 'b'{}".format(data))
            self.read_update(data)

    def read_update(self, data):
        if self.path in bots:
            bots[self.path.lstrip('/')].process_new_updates([telebot.types.Update.de_json(data)])

    def log_message(self, format, *args):
        return None
