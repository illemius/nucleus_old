# Settings sample
```python
TELEBOT = {
    "WEBHOOK": {
        "ENABLED": True,
        "HOST": "",
        "PORT": 15793,
        "URL_BASE": "https://example.com"
    },
    "BOTS": {
        "Main": {
            "TOKEN": "241330243:AAG3G3WqaiEFa7KN6WxD357Cr1uWU78gMhU",
            "SKIP_PENDING": True,
            "ROOT_USER": 1234567890,
            "USE_WEBHOOK": True,
        }
    }
}
```