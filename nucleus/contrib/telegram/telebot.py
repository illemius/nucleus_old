import re

import telebot

from NucleusUtils.events import Event


class TelegramBot(telebot.TeleBot):
    def __init__(self, token, threaded=True, skip_pending=False):
        super(TelegramBot, self).__init__(token, threaded, skip_pending)
        self._me = None
        self.webhook_url = None

        self.root_user_id = 0

        event_name = '@' + self.me.username + ':'
        self.on_new_updates = Event(name=event_name + 'on_new_updates')
        self.on_new_message = Event(name=event_name + 'on_new_message')
        self.on_edited_new_message = Event(name=event_name + 'on_edited_new_message')
        self.on_new_channel_post = Event(name=event_name + 'on_new_channel_post')
        self.on_new_edited_channel_post = Event(name=event_name + 'on_new_edited_channel_post')
        self.on_new_inline_query = Event(name=event_name + 'on_new_inline_query')
        self.on_new_chosen_inline_result = Event(name=event_name + 'on_new_chosen_inline_result')
        self.on_new_callback_query = Event(name=event_name + 'on_new_callback_query')

    def set_root(self, user_id):
        self.root_user_id = user_id

    def get_root(self):
        return self.root_user_id if self.root_user_id > 0 else None

    @property
    def me(self) -> telebot.types.User:
        if self._me is None:
            self._me = self.get_me()
        return self._me

    def polling(self, none_stop=False, interval=0, timeout=20):
        try:
            super(TelegramBot, self).polling(none_stop, interval, timeout)
        except (KeyboardInterrupt, SystemExit):
            pass
        except:
            super(TelegramBot, self).polling(none_stop, interval, timeout)

    def skip_updates(self):
        """
        Get and discard all pending updates before first poll of the bot
        :return: total updates skipped
        """
        total = 0
        updates = self.get_updates(offset=self.last_update_id, timeout=1)
        while updates:
            total += len(updates)
            for update in updates:
                if update.update_id > self.last_update_id:
                    self.last_update_id = update.update_id
            updates = self.get_updates(offset=self.last_update_id + 1, timeout=1)
        return total

    @telebot.util.async()
    def send_message(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                     parse_mode=None, disable_notification=None):
        super(TelegramBot, self).send_message(chat_id, text, disable_web_page_preview=None, reply_to_message_id=None,
                                              reply_markup=None, parse_mode=None, disable_notification=None)

    @telebot.util.async()
    def forward_message(self, chat_id, from_chat_id, message_id, disable_notification=None):
        super(TelegramBot, self).forward_message(chat_id, from_chat_id, message_id, disable_notification=None)

    @telebot.util.async()
    def send_audio(self, chat_id, audio, caption=None, duration=None, performer=None, title=None,
                   reply_to_message_id=None,
                   reply_markup=None, disable_notification=None, timeout=None):
        return super(TelegramBot, self).send_audio(chat_id, audio, caption=None, duration=None, performer=None,
                                                   title=None, reply_to_message_id=None, reply_markup=None,
                                                   disable_notification=None, timeout=None)

    @telebot.util.async()
    def send_chat_action(self, chat_id, action):
        return super(TelegramBot, self).send_chat_action(chat_id, action)

    @telebot.util.async()
    def send_contact(self, chat_id, phone_number, first_name, last_name=None, disable_notification=None,
                     reply_to_message_id=None, reply_markup=None):
        return super(TelegramBot, self).send_contact(chat_id, phone_number, first_name, last_name=None,
                                                     disable_notification=None,
                                                     reply_to_message_id=None, reply_markup=None)

    @telebot.util.async()
    def send_document(self, chat_id, data, reply_to_message_id=None, caption=None, reply_markup=None,
                      disable_notification=None, timeout=None):
        return super(TelegramBot, self).send_document(chat_id, data, reply_to_message_id=None, caption=None,
                                                      reply_markup=None, disable_notification=None, timeout=None)

    @telebot.util.async()
    def send_game(self, chat_id, game_short_name, disable_notification=None, reply_to_message_id=None,
                  reply_markup=None):
        return super(TelegramBot, self).send_game(chat_id, game_short_name, disable_notification=None,
                                                  reply_to_message_id=None, reply_markup=None)

    @telebot.util.async()
    def send_location(self, chat_id, latitude, longitude, reply_to_message_id=None, reply_markup=None,
                      disable_notification=None):
        return super(TelegramBot, self).send_location(chat_id, latitude, longitude, reply_to_message_id=None,
                                                      reply_markup=None, disable_notification=None)

    @telebot.util.async()
    def send_photo(self, chat_id, photo, caption=None, reply_to_message_id=None, reply_markup=None,
                   disable_notification=None):
        return super(TelegramBot, self).send_photo(chat_id, photo, caption=None, reply_to_message_id=None,
                                                   reply_markup=None, disable_notification=None)

    @telebot.util.async()
    def send_sticker(self, chat_id, data, reply_to_message_id=None, reply_markup=None, disable_notification=None,
                     timeout=None):
        return super(TelegramBot, self).send_sticker(chat_id, data, reply_to_message_id=None, reply_markup=None,
                                                     disable_notification=None, timeout=None)

    @telebot.util.async()
    def send_venue(self, chat_id, latitude, longitude, title, address, foursquare_id=None, disable_notification=None,
                   reply_to_message_id=None, reply_markup=None):
        return super(TelegramBot, self).send_venue(chat_id, latitude, longitude, title, address, foursquare_id=None,
                                                   disable_notification=None, reply_to_message_id=None,
                                                   reply_markup=None)

    @telebot.util.async()
    def send_video(self, chat_id, data, duration=None, caption=None, reply_to_message_id=None, reply_markup=None,
                   disable_notification=None, timeout=None):
        return super(TelegramBot, self).send_video(chat_id, data, duration=None, caption=None, reply_to_message_id=None,
                                                   reply_markup=None, disable_notification=None, timeout=None)

    @telebot.util.async()
    def send_voice(self, chat_id, voice, caption=None, duration=None, reply_to_message_id=None, reply_markup=None,
                   disable_notification=None, timeout=None):
        return super(TelegramBot, self).send_voice(chat_id, voice, caption=None, duration=None,
                                                   reply_to_message_id=None, reply_markup=None,
                                                   disable_notification=None, timeout=None)

    @telebot.util.async()
    def set_game_score(self, user_id, score, force=None, chat_id=None, message_id=None, inline_message_id=None,
                       edit_message=None):
        return super(TelegramBot, self).set_game_score(user_id, score, force=None, chat_id=None, message_id=None,
                                                       inline_message_id=None, edit_message=None)

    @telebot.util.async()
    def set_webhook(self, url=None, certificate=None, max_connections=None, allowed_updates=None):
        self.webhook_url = url
        return super(TelegramBot, self).set_webhook(url=None, certificate=None, max_connections=None,
                                                    allowed_updates=None)

    @telebot.util.async()
    def answer_callback_query(self, callback_query_id, text=None, show_alert=None, url=None, cache_time=None):
        return super(TelegramBot, self).answer_callback_query(callback_query_id, text=None, show_alert=None, url=None,
                                                              cache_time=None)

    @telebot.util.async()
    def answer_inline_query(self, inline_query_id, results, cache_time=None, is_personal=None, next_offset=None,
                            switch_pm_text=None, switch_pm_parameter=None):
        return super(TelegramBot, self).answer_inline_query(inline_query_id, results, cache_time=None, is_personal=None,
                                                            next_offset=None, switch_pm_text=None,
                                                            switch_pm_parameter=None)

    def _test_filter(self, filter, filter_value, message):
        test_cases = {
            'content_types': lambda msg: msg.content_type in filter_value,
            'regexp': lambda msg: msg.content_type == 'text' and re.search(filter_value, msg.text),
            'commands': lambda msg:
            msg.content_type == 'text' and extract_command(msg.text, self.me.username) in filter_value,
            'func': lambda msg: filter_value(msg)
        }

        return test_cases.get(filter, lambda msg: False)(message)

    def process_new_updates(self, updates):
        self.on_new_updates(updates)
        return super(TelegramBot, self).process_new_updates(updates)

    def process_new_messages(self, new_messages):
        for message in new_messages:
            self.on_new_message(message)
        return super(TelegramBot, self).process_new_messages(new_messages)

    def process_new_edited_messages(self, edited_message):
        for message in edited_message:
            self.on_edited_new_message(message)
        return super(TelegramBot, self).process_new_edited_messages(edited_message)

    def process_new_channel_posts(self, channel_post):
        self.on_new_channel_post(channel_post)
        super(TelegramBot, self).process_new_channel_posts(channel_post)

    def process_new_edited_channel_posts(self, edited_channel_post):
        self.on_new_edited_channel_post(edited_channel_post)
        return super(TelegramBot, self).process_new_edited_channel_posts(edited_channel_post)

    def process_new_inline_query(self, new_inline_querys):
        for query in new_inline_querys:
            self.on_new_inline_query(query)
        return super(TelegramBot, self).process_new_inline_query(new_inline_querys)

    def process_new_chosen_inline_query(self, new_chosen_inline_querys):
        for query in new_chosen_inline_querys:
            self.on_new_chosen_inline_result(query)
        return super(TelegramBot, self).process_new_chosen_inline_query(new_chosen_inline_querys)

    def process_new_callback_query(self, new_callback_querys):
        for query in new_callback_querys:
            self.on_new_callback_query(query)
        return super(TelegramBot, self).process_new_messages(new_callback_querys)


def extract_command(text, bot_username):
    cmd = text.split()[0].split('@')
    if telebot.util.is_command(cmd[0]):
        if len(cmd) == 1:
            return cmd[0][1:]
        if cmd[1].lower() == bot_username.lower():
            return cmd[0][1:]
    return None
