import os

if __name__ == '__main__':
    os.environ.setdefault("NUCLEUS_SETTINGS", "app.settings")
    try:
        import nucleus
    except ImportError:
        raise ImportError('Module "nucleus" is not installed!')
    else:
        nucleus.setup()
