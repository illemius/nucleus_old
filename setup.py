from distutils.core import setup

from setuptools import PackageFinder

import nucleus

LIB_NAME = 'nucleus'

setup(
    name='nucleuscore',
    version=nucleus.__version__,
    packages=[LIB_NAME] + list(map(lambda name: LIB_NAME + '.' + name, PackageFinder.find(LIB_NAME))),
    url='',
    license='q',
    author='Alex Root Junior',
    author_email='jroot.junior@gmail.com',
    description=''
)
