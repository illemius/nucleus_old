import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

APPLICATION_NAME = "TestApp"

INSTALLED_APPS = (
    "nucleus.contrib.logger",
    "nucleus.contrib.telegram",
)

LOGGING = {
    "LOG_LEVEL": "INFO",
    "SETUP_LOGGERS": [
        "app",
        "dumps",
        "TeleBot",
        "WebHookServer"
    ],
    "REPORTS_STORAGE": os.path.join(BASE_DIR, 'reports'),
    "HANDLERS": [
        {
            "HANDLER": "logging.StreamHandler",
            "FORMATTER": '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
        }
    ]
}

TELEBOT = {
    "WEBHOOK": {
        "ENABLED": True,
        "HOST": "",
        "PORT": 15793,
        "URL_BASE": "https://example.com"
    },
    "BOTS": {
        APPLICATION_NAME: {
            "TOKEN": "241330243:AAG3G3WqaiEFa7KN6WxD357Cr1uWU78gMhU",
            "SKIP_PENDING": True,
            "ROOT_USER": 1234567890,
            "USE_WEBHOOK": True,
        }
    }
}
